# Abstract:
The internet offers a lot of information and since the concept of the cloud these are also accessible via APIs. Today there is hardly any service which is not available as an API.
This raises the question: "Is it possible to provide a productive application in the cloud, based alone on free available APIs?"
We will check it out an and together develop a REST application based on Java and Spring Boot.
Other languages ​​and technologies are also cordially invited.

For further information and preparations see <https://goo.gl/8I45Wn>

## Still in work! -.-

## Workshop
The workshop is devided into the following objectives and branches:
1. Create an initial SpringBoot Application.<br/>Branch "master"
2. Create a first REST-API and host the application on Heroku.<br/>Branch "0.0.1"
3. Connect your application with the register and integrate Loogly.<br/>Branch "0.0.2"
4. Integrate the App42 Cloud, save your orders there and integrate their logging.<br/>Branch "0.0.3"

## Necessary accounts
We are using the apis of the following services. Please create an account on each platform:
* Heroku: https://www.heroku.com
* Loggly: https://www.loggly.com
* App42 Cloud: http://api.shephertz.com

## Build and run the application
1. You can start the programm by execute the Main.class in your IDE like an normal Java-Application.
2. Build a runnable jar with "**gradle build**" and execute with "**java -jar manufacturer-1.0-SNAPSHOT.jar**"